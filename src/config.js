export var firebaseConfig = {
    apiKey: 'AIzaSyBoUdiDtzQdC-m4nj9CPY5SvY7uKJGL71k',
    authDomain: 'matx-15ede.firebaseapp.com',
    databaseURL: 'https://matx-15ede.firebaseio.com',
    projectId: 'matx-15ede',
    storageBucket: 'matx-15ede.appspot.com',
    messagingSenderId: '348111707030',
    appId: '1:348111707030:web:70c4ca4eb3f1dbd18e1bb7',
    measurementId: 'G-806629YLNN',
}

export const auth0Config = {
    client_id: 'XmminWIs0S8gR3gIRBydYLWbF58x81vK',
    domain: 'matx.us.auth0.com',
}

/*
*
* WE DON'T KNOW WHY THE CONFIGS ABOVE MATTERS AND WHAT THEY DOES...
* BUT BELOW ARE CONFIGS THAT YOU NEED.
* THEY ARE ABOUT CONNECTING BACK-END APPLICATION TO THIS CLIENT APP.
* PLEASE, DO NOT DELETE ANYTHING ANYWAY, IF YOU DON'T FINALLY KNOW WHAT YOU'RE LITERALLY DO!
* (WE DIDN'T FOUND ANY USAGES OF THEM TOO :D)
*
* */

export const appConfig = {
    // apiDomain: 'http://127.0.0.1:5000/',
    apiDomain: 'http://109.191.214.169:5000/',
    // apiDomain: 'http://26.104.6.128:5000/',
    // apiDomain: 'http://192.168.0.109:5000/',
    logoUrl: 'https://avatars.mds.yandex.net/get-dialogs/758954/ed5da2e5235a766fbf95/catalogue-icon-x3',
    serviceName: 'IS74-PR'
}