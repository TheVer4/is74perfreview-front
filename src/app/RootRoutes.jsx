import React from 'react'
import { Redirect } from 'react-router-dom'

import managerRoutes from './views/manager/ManagerRoutes'
import prRoutes from './views/pr/PrRoutes'

import dashboardRoutes from './views/dashboard/DashboardRoutes'
import utilitiesRoutes from './views/utilities/UtilitiesRoutes'

import materialRoutes from './views/material-kit/MaterialRoutes'
import chartsRoute from './views/charts/ChartsRoute'
import dragAndDropRoute from './views/Drag&Drop/DragAndDropRoute'

import formsRoutes from './views/forms/FormsRoutes'
import mapRoutes from './views/map/MapRoutes'
import userPagesRoutes from "./views/UserPages/UserPagesRoutes";
import taskfeedRoutes from "./views/taskfeed/TaskfeedRoutes";
import userSettingsRoutes from "./views/userSettings/userSettingsRoute";


const redirectRoute = [
    {
        path: '/',
        exact: true,
        component: () => <Redirect to="/taskfeed" />,
    },
]

const errorRoute = [
    {
        component: () => <Redirect to="/session/404" />,
    },
]

const routes = [
    ...dashboardRoutes,
    ...materialRoutes,
    ...utilitiesRoutes,
    ...chartsRoute,
    ...dragAndDropRoute,
    ...formsRoutes,
    ...mapRoutes,
    ...managerRoutes,
    ...prRoutes,
    ...userPagesRoutes,
    ...taskfeedRoutes,
    ...userSettingsRoutes,
    ...redirectRoute,
    ...errorRoute,
]

export default routes
