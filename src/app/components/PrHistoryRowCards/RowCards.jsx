import React, { Fragment } from 'react'
import { format } from 'date-fns'
import {
    Grid,
    Card,
    Icon,
    IconButton,
    Avatar,
    Hidden, Box,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import {AvatarGroup, Rating} from "@material-ui/lab";

const useStyles = makeStyles(({ palette, ...theme }) => ({
    projectName: {
        marginLeft: 24,
        [theme.breakpoints.down('sm')]: {
            marginLeft: 4,
        },
    },
}))

const labels = {
    0: 'Затрудняюсь ответить',
    1: 'Значительно ниже ожиданий',
    2: 'Ниже ожиданий',
    3: 'Соответствует ожиданиям',
    4: 'Выше ожиданий',
    5: 'Значительно выше ожиданий',
};

const RowCards = (props) => {
    const classes = useStyles()

    console.log(props.reviews);

    return props.reviews.map((val, id) => (
        <Fragment key={id}>
            <Card className="py-2 px-4 project-card">
                <Grid container alignItems="center">
                    <Grid item md={5} xs={7}>
                        <div className="flex items-center">
                            <Hidden smDown>
                                <Rating
                                    readOnly
                                    className="ml-4"
                                    precision={1}
                                    value={val.total_mark}
                                    size="large"
                                />
                            </Hidden>
                            <span
                                className={clsx(
                                    'font-medium',
                                    classes.projectName
                                )}
                            >
                                {4 !== null && (
                                    <Box sx={{ ml: 2 }}>{val.total_mark ? labels[val.mark] : "Невозможно сказать"}</Box>
                                )}
                            </span>
                        </div>
                    </Grid>

                    <Grid item md={3} xs={4}>
                        <div className="text-muted">
                            {format(val.date_start * 1000, 'dd/MM/yyyy')}
                        </div>
                    </Grid>

                    <Hidden smDown>
                        <Grid item xs={3}>
                            {/*<AvatarGroup max={4}>*/}
                            {/*{*/}
                            {/*    val.reviewers.map((rev, revind) =>*/}
                            {/*        <Avatar*/}
                            {/*            alt={rev.avatar}*/}
                            {/*            src={rev.avatar}*/}
                            {/*        />*/}
                            {/*    )*/}
                            {/*}*/}
                            {/*</AvatarGroup>*/}
                        </Grid>
                    </Hidden>

                    <Grid item xs={1}>
                        <div className="flex justify-end">
                            <IconButton onClick={(e) => { document.location.href = `/pr/view/${val.id}`}}>
                                <Icon>send</Icon>
                            </IconButton>
                        </div>
                    </Grid>
                </Grid>
            </Card>
            <div className="py-2" />
        </Fragment>
    ))
}

export default RowCards
