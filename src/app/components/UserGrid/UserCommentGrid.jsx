import * as React from 'react';
import styled  from 'styled-components';
import {Grid, Typography, Chip, Box, Avatar as MuiAvatar} from '@material-ui/core';
import {Rating, Skeleton} from "@material-ui/lab";

const Avatar = styled(MuiAvatar)({
    width: 132,
    height: 132
});

const Card = styled('div')({
    width: '100%',
    margin: '15px;'
});

export default function UserCommentGrid(props) {
    return (
        <Card>
            {/*<Paper sx={{ p: 2, margin: 'auto', flexGrow: 1 }}>*/}
                <Grid container spacing={2}>
                    <Grid item direction={"column"} justify={"center"}>
                        { props.image === 'loading'
                            ? <Skeleton variant="circular"><Avatar/></Skeleton>
                            : <Avatar src={props.image}/>
                        }
                    </Grid>
                    <Grid item xs={12} sm container>
                        <Grid item xs container direction="column" spacing={2}>
                            <Grid item xs>
                                <Typography gutterBottom variant="subtitle1" component="div">
                                    {props.fullname}
                                </Typography>
                                <Typography variant="body2" gutterBottom>
                                    {props.children}
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography sx={{ cursor: 'pointer' }} variant="body2">
                                    {props.mark === null
                                        ? <div/>
                                        : <Box spacing={2}>
                                            <Typography component="legend">Оценка</Typography>
                                            <Rating defaultValue={props.mark} precision={0.5} readOnly/>
                                          </Box>
                                    }
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Typography variant="subtitle1" component="div">
                                {props.date}
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            {/*</Paper>*/}
        </Card>
    );
}