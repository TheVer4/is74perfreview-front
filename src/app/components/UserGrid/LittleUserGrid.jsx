import * as React from 'react';
import styled  from 'styled-components';
import {Grid, Typography, Avatar as MuiAvatar, TextField} from '@material-ui/core';
import {Autocomplete} from "@material-ui/lab";

const Avatar = styled(MuiAvatar)({
    width: 110,
    height: 110
});

const Card = styled('div')({
    width: '100%',
    margin: '0px;',
});

export default function LittleUserGrid(props) {

    return (
        <Card>
            <Grid container spacing={2}>
                <Grid item direction={"column"} justify={"center"}>
                    <Avatar src={props.image} />
                </Grid>
                <Grid item xs={12} sm container>
                    <Grid item xs container direction="column" spacing={2}>
                        <Grid item xs>
                            <Typography gutterBottom variant="subtitle1" component="div">
                                {props.fullname}
                            </Typography>
                            <Autocomplete
                                className="mb-2 w-25"
                                options={props.categories}
                                getOptionLabel={(option) => option.label}
                                onChange={(e, val) => {props.userCategory.boxed = val}}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        label="Категория"
                                        variant="outlined"
                                        value={props.userCategory.boxed} //TODO сделать чтобы value было не константным, а одним из state
                                        fullWidth
                                    />
                                )}
                            />
                            <Typography variant="body2" color="text.secondary">
                                ID: {props.userId}
                                <br/>
                                E-Mail: {props.email}
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Card>
    );
}