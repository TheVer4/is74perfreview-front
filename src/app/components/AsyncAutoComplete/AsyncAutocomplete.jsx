import fetch from 'cross-fetch'
import React from 'react'
import { TextField, CircularProgress } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'

export default function AsyncAutocomplete(props) {
    const [open, setOpen] = React.useState(false)
    const [options, setOptions] = React.useState([])
    const loading = open && options.length === 0

    React.useEffect(() => {
        let active = true

        if (!loading) {
            return undefined
        }

        ;(async () => {
            const response = await fetch(
                'http://localhost:5000/getUsersByDepartment' //TODO Придумать как сделать поиск интуитивно-понятным
            )
            const countries = await response.json()

            if (active) {
                console.log(countries);
                setOptions(
                    Object.keys(countries).map((key) => countries[key].item[0])
                )
            }
        })()

        return () => {
            active = false
        }
    }, [loading])

    React.useEffect(() => {
        if (!open) {
            setOptions([])
        }
    }, [open])

    return (
        <Autocomplete
            id="asynchronous-demo"
            className="w-300"
            open={open}
            onOpen={() => {
                setOpen(true)
            }}
            onClose={() => {
                setOpen(false)
            }}
            getOptionSelected={(option, value) => option.name === value.name}
            getOptionLabel={(option) => option.name}
            options={options}
            loading={loading}
            renderInput={(params) => (
                <TextField
                    {...params}
                    label={props.label}
                    fullWidth
                    variant="outlined"
                    onChange={props.onChange}
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <React.Fragment>
                                {loading ? (
                                    <CircularProgress
                                        color="inherit"
                                        size={20}
                                    />
                                ) : null}
                                {params.InputProps.endAdornment}
                            </React.Fragment>
                        ),
                    }}
                />
            )}
        />
    )
}
