import React from 'react'
import useSettings from 'app/hooks/useSettings'
import {appConfig} from "../../../config";

const Is74Logo = (/*{ className }*/) => {
    // const { settings } = useSettings()
    // const theme = settings.themes[settings.activeTheme]

    return (
        <img src={appConfig.logoUrl}
             alt={'Интерсвязь:Перфоманс-ревью'}
             width={24}
             height={24}
             style={{'borderRadius': '5px'}}/>
    )
}

export default Is74Logo
