export const authRoles = {
    admin: ['ADMIN'], // Only Super Admin has access
    depLeader: ['ADMIN', 'DEP_LEADER'], // Only SA & Admin has access
    moderator: ['ADMIN', 'DEP_LEADER', 'MODERATOR'], // Only SA & Admin & Editor has access
    employee: ['ADMIN', 'DEP_LEADER', 'MODERATOR', 'EMPLOYEE'], // Everyone has access
}

// Check out app/views/dashboard/DashboardRoutes.js
// Only SA & Admin has dashboard access

// const dashboardRoutes = [
//   {
//     path: "/dashboard/analytics",
//     component: Analytics,
//     auth: authRoles.admin <===============
//   }
// ];

// Check navigaitons.js

// {
//   name: "Dashboard",
//   path: "/dashboard/analytics",
//   icon: "dashboard",
//   auth: authRoles.admin <=================
// }
