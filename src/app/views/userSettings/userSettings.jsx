import React, {useState, useEffect} from 'react'
import { Breadcrumb, SimpleCard } from 'app/components'
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
} from '@material-ui/core'
import axios from "../../../axios";
import {appConfig} from "../../../config";
import DialogContentText from "@material-ui/core/DialogContentText";

const UserSettings = () => {

    const [during, updateDuring] = useState([]);
    const [openDialogStartPR, setOpenDialogStartPR] = useState(false);

    const updateAllLists = () => {
        loadData('getUsersDuringNowPR', updateDuring).then(r => {});
    };

    useEffect(updateAllLists, [])

    const loadData = async (url, update) => {
        try {
            const res = await axios.get( appConfig.apiDomain + url)
            console.log(url);
            console.log(res.data);
            update(res.data);
        } catch (e) {
            console.error(e)
        }
    }

    const sendData = async (url, data, update) => {
        try {
            const res = await axios.post( appConfig.apiDomain + url, data)
            update(res.data);
        } catch (e) {
            console.error(e)
        }
    }

    const handleClickOpenDialogStartPR = () => {
        setOpenDialogStartPR(true);
    };

    const handleCloseDialogStartPR = () => {
        setOpenDialogStartPR(false);
    };


    return (
        <div className="m-sm-30">
            <div className="mb-sm-30 flex justify-between">
                <div className="flex ">
                    <Breadcrumb
                        routeSegments={[
                            { name: 'Настройки', path: '/settings' },
                        ]}
                    />
                </div>
            </div>
            <SimpleCard title="Настройки пользователя">
                <Button onClick={handleClickOpenDialogStartPR}>Сохранить</Button>
                <Dialog
                    open={openDialogStartPR}
                    keepMounted
                    onClose={handleCloseDialogStartPR}
                    aria-describedby="alert-dialog-slide-description"
                >
                    <DialogTitle>{"Уточните действие"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-slide-description">
                            Вы действительно хотите внести указанные изменения?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button variant="outlined" color='error' onClick={handleCloseDialogStartPR}>Отмена</Button>
                        <Button color="primary" onClick={() => {
                            sendData(`addDuringPr`, {
                                //TODO PAYLOAD
                            }, x => {}).then(x => {
                                updateAllLists();
                            });
                            handleCloseDialogStartPR();
                        }}>Однозначно!</Button>
                    </DialogActions>
                </Dialog>
            </SimpleCard>
            <div className="py-3" />
        </div>
    )
}

export default UserSettings;
