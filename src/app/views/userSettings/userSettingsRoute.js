import React from 'react'

const userSettingsRoutes = [
    {
        path: '/settings',
        component: React.lazy(() => import('./userSettings')),
    },
]

export default userSettingsRoutes
