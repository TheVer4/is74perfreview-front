import React from 'react'

const taskfeedRoutes = [
    {
        path: '/taskfeed',
        component: React.lazy(() => import('./Taskfeed')),
    },
]

export default taskfeedRoutes
