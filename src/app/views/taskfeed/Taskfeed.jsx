import React, {useState, useEffect} from 'react'
import { Breadcrumb, SimpleCard, UserGrid } from 'app/components'
import {
    Box,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Grid,
    Icon, Typography
} from '@material-ui/core'
import axios from "../../../axios";
import {appConfig} from "../../../config";
import DialogContentText from "@material-ui/core/DialogContentText";
import {NavLink} from "react-router-dom";
import DateFnsUtils from "@date-io/date-fns";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import Chip from "@material-ui/core/Chip";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { getTimeDifference } from 'utils.js'
import {differenceInSeconds} from "date-fns";

const Taskfeed = () => {

    const [tasklist, updateTasklist] = useState([]);
    
    const updateAllLists = () => {
        loadData('getTodos', fillTasklist).then(r => {});
    };

    const fillTasklist = (data) => {
        console.log(data)
        updateTasklist(data.map((task, ind) => {{
            return {
                title: getTitle(task.type),
                content: getContent(task.type, task.what),
                date: task.what.date_start,
                url: getButtonUrl(task.type, task.what),
                buttonText: getButtonText(task.type),
                buttonIcon: getButtonIcon(task.type),
            }
        }}))
        console.log(tasklist)
    };

    const getTitle = (type) => {
        switch (type) {
            case 'WAITING_FOR_SR':
                return "Вам назначили Performance review"
            case 'INVITATION_TO_PR':
                return "Вас пригласили оставить отзыв"
            case 'WAITING_FOR_CLAIM':
                return "PR Ищет модератора"
            case 'WAITING_FOR_MODERATOR':
                return "Ожидается реакция модератора"
            default:
                return "Версия приложения устарела"
        }
    };

    const getContent = (type, payload) => {
        switch (type) {
            case 'WAITING_FOR_SR':
                return payload.who_started.fullname + " назначил вам прохождение PR. В ближайшее время нужно заполнить Self review и выбрать ревьюеров. Позже, Ваш отчёт направится модератору на проверку."
            case 'INVITATION_TO_PR':
                return "Вы были приглашены оставить отзыв о работе Вашего коллеги: " + payload.during_pr.about_who.fullname + ". Это сообщение не исчезнет из Вашей ленты до тех пор, пока вы этого не сделаете!"
            case 'WAITING_FOR_CLAIM':
                return "Этот PR (В отношении пользователя " + payload.about_who.fullname + ") был запущен автоматически и ожидает чтобы кто-нибудь стал его курировать. Вы можете приурочить его себе!"
            case 'WAITING_FOR_MODERATOR':
                return "В этом PR был заполнен Self Review. Обратите на него внимание!"
            default:
                return "Обратитесь в отдел внедрения и сообщите о том, что вы видите это сообщение. Это означает что тип уведомления не поддерживается текущей версией клиента."
        }
    };

    const getButtonUrl = (type, payload) => {
        switch (type) {
            case 'WAITING_FOR_SR':
                return `/pr/addUp/${payload.id}`
            case 'INVITATION_TO_PR':
                return `/pr/leaveReview/${payload.id}`
            case 'WAITING_FOR_CLAIM':
                return "/УБОЙНАЯФИЧАКОТОРОЙНЕТ" //TODO Сделать клайминг этих пиаров
            case 'WAITING_FOR_MODERATOR':
                return `/pr/view/${payload.id}`
            default:
                return `https://is74.ru/`
        }
    };

    const getButtonText = (type) => {
        switch (type) {
            case 'WAITING_FOR_SR':
                return `Заполнить Self Review`
            case 'INVITATION_TO_PR':
                return `Оставить ревью`
            case 'WAITING_FOR_CLAIM':
                return "Присвоить модераторство"
            case 'WAITING_FOR_MODERATOR':
                return "Перейти к просмотру действий"
            default:
                return `Обратиться`
        }
    };

    const getButtonIcon = (type) => {
        switch (type) {
            case 'WAITING_FOR_SR':
                return `create`
            case 'INVITATION_TO_PR':
                return `record_voice_over`
            case 'WAITING_FOR_CLAIM':
                return `device_hub`
            default:
                return `send`
        }
    };

    useEffect(updateAllLists, [])

    const loadData = async (url, update) => {
        try {
            const res = await axios.get( appConfig.apiDomain + url)
            update(res.data);
        } catch (e) {
            console.error(e)
        }
    }

    return (
        <div className="m-sm-30">
            <div className="mb-sm-30 flex justify-between">
                <div className="flex ">
                    <Breadcrumb
                        routeSegments={[
                            { name: 'Список задач', path: '/taskfeed' },
                        ]}
                    />
                </div>
                <Button variant={"outlined"} className={'flex'} startIcon={<Icon>refresh</Icon>} onClick={updateAllLists}>Обновить</Button>
            </div>
            <SimpleCard title="Ваш список задач на ближайшее время">
                <Grid container direction={"column"} spacing={3}>
                    { tasklist.length !== 0
                        ? tasklist.map((bg, ind) => (
                                <Grid item key={ind}>
                                    <div
                                        className={`h-145 w-100% border-radius-8 elevation-z6 bg-default flex justify-center items-center`}
                                    >
                                        <Grid container className={'p-4'}>
                                            <Grid item xs={12} sm container>
                                                <Grid item xs container direction="column" spacing={2}>
                                                    <Grid item xs>
                                                        <Typography gutterBottom variant="h4" component="div">
                                                            {bg.title}
                                                        </Typography>
                                                        <Typography variant="body2" gutterBottom>
                                                            {bg.content}
                                                        </Typography>
                                                        <Typography variant="body2" color="text.secondary">
                                                            Нажмите кнопку ниже, чтобы перейти
                                                        </Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        <Typography sx={{ cursor: 'pointer' }} variant="body2">
                                                            <Box spacing={2}>
                                                                <Button color="primary" onClick={() => { document.location.href = bg.url; }} variant="contained" startIcon={<Icon>{bg.buttonIcon}</Icon>}>{bg.buttonText}</Button>
                                                            </Box>
                                                        </Typography>
                                                    </Grid>
                                                </Grid>
                                                <Grid item>
                                                    <Typography variant="subtitle1" component="div">
                                                        Получено: <Chip label={getTimeDifference(bg.date*1000) + ' назад'}/>
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </div>
                                </Grid>
                            ))
                        : <Grid item>
                            <div
                                className={`h-145 w-100% flex justify-center items-center`}
                            >
                                Вам не назначено каких-либо задач :)<br/>
                                Можно отдохнуть от этого приложения и, сделать себе кофе, например
                            </div>
                        </Grid>
                    }
                </Grid>
            </SimpleCard>
        </div>
    )
}

export default Taskfeed
