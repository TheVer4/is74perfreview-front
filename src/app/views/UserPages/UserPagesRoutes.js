import React from 'react'

const userPagesRoutes = [
    {
        path: '/user/:user_id',
        component: React.lazy(() => import('./User')),
    },
]

export default userPagesRoutes
