import React, {Fragment, useEffect, useState} from 'react'
import {Grid, Card, Avatar as MuiAvatar, Typography, Box, Tooltip} from '@material-ui/core'

import { useTheme } from '@material-ui/styles'
import {useParams} from "react-router-dom";
import useAuth from "../../hooks/useAuth";
import styled from "styled-components";
import RowCards from "../../components/PrHistoryRowCards/RowCards";
import axios from "../../../axios";
import {appConfig} from "../../../config";

const User = () => {

    const { user_id } = useParams();

    const { user } = useAuth();
    const [userInfo, updateUserInfo] = useState(user);
    const [comrades, updateComrades] = useState([]);
    const [reviews, updateReviews] = useState([]);


    const [sizedAva, updateSizedAva] = useState({
        width: 110,
        height: 110,
    });
    const Avatar = styled(MuiAvatar)(sizedAva);

    useEffect(() => {
        console.log(user);
        loadData('/getComradesOfUser', updateComrades).then(r => {});
        loadData(`/getAllUserPrs?id=${user_id}`, updateReviews).then(r => {});
        loadData(`/getUserData?id=${user_id}`, updateUserInfo).then(r => {})
        let bounds = document.getElementById('AvatarCard').getBoundingClientRect();
        // noinspection JSSuspiciousNameCombination
        updateSizedAva({
            width: bounds.width*0.9,
            height: bounds.width*0.9,
        });
        if(user.id !== user_id) {
            //updateUserInfo();
        }
    }, []);

    const loadData = async (url, update) => {
        try {
            const res = await axios.get( appConfig.apiDomain + url)
            update(res.data);
        } catch (e) {
            console.error(e)
        }
    }

    const theme = useTheme()

    return (
        <Fragment>
            <div className="analytics m-sm-30 mt-6">
                <Grid container spacing={3}>
                    <Grid item lg={9} md={9} sm={12} xs={12}>
                        <Card className="px-6 py-4 mb-6">
                            <Typography variant="h3" gutterBottom component="div">
                                {userInfo.fullname}
                            </Typography>
                            <Grid container className='flex justify-between'>
                                <Grid item className='flex'>Почта:</Grid>
                                <Grid item className='flex'>{userInfo.email}</Grid>
                                <Grid item className='flex'></Grid>
                            </Grid>
                            <Grid container className='flex justify-between'>
                                <Grid item className='flex'>Должность:</Grid>
                                <Grid item className='flex'>{userInfo.position}</Grid>
                                <Grid item className='flex'></Grid>
                            </Grid>
                            <Grid container className='flex justify-between'>
                                <Grid item className='flex'>Отдел:</Grid>
                                <Grid item className='flex'>{userInfo.department}</Grid>
                                <Grid item className='flex'></Grid>
                            </Grid>
                        </Card>
                        <h4 className="card-title text-muted mb-4 mx-4">
                            История развития
                        </h4>
                        <RowCards reviews={reviews}/>
                    </Grid>

                    <Grid item lg={3} md={3} sm={12} xs={12}>
                        <Card className="px-6 py-4 mb-6" id="AvatarCard">
                            {/*<div className="card-subtitle">За последние 30 дней</div>*/}
                             <Avatar src={`${userInfo.avatar}`}/>

                        </Card>
                        <Card className="px-6 py-4 mb-6" id="AvatarCard">
                            <div className="card-title mb-4">Коллеги по цеху</div>
                            {/*<div className="card-subtitle">За последние 30 дней</div>*/}
                            <Grid container spacing={2}>
                                {
                                    comrades.map((val, ind) =>
                                        <Grid item><Tooltip title={val.fullname}><a href={`/user/${val.id}`}><MuiAvatar src={val.avatar}/></a></Tooltip></Grid> //TODO Use val instead of userInfo
                                    )
                                }
                            </Grid>

                        </Card>
                    </Grid>
                </Grid>
            </div>
        </Fragment>
    )
}

export default User
