import React, {useState, useEffect} from 'react'
import { Breadcrumb, SimpleCard, UserGrid } from 'app/components'
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Grid,
    Icon
} from '@material-ui/core'
import axios from "../../../axios";
import {appConfig} from "../../../config";
import DialogContentText from "@material-ui/core/DialogContentText";
import {NavLink} from "react-router-dom";
import DateFnsUtils from "@date-io/date-fns";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import { getTimeDifference } from 'utils.js'
import {differenceInSeconds} from "date-fns";

const DEFAULT_SUBJECT_FAILURE = {
    user: {
        fullname: undefined,
        id: undefined,
    }
};

const DEFAULT_SUBJECT_ALREADY_FAILURE = {
    about_who: {
        fullname: undefined,
        id: undefined,
    }
};

const TRANSLATE_STATUS = {
    'prepare': 'Ожидание SR',
    'self review': 'Ожидание реакции модератора',
    'performance review': 'Ожидание заполнения респондентами',
    'verification': 'Изменять больше нельзя',
    'closed': 'Закрыто'
};

const Queue = () => {

    const [requiredSoon, updateRequiredSoon] = useState([]);
    const [during, updateDuring] = useState([]);
    const [openDialogStartPR, setOpenDialogStartPR] = useState(false);
    const [openDialogEndPR, setOpenDialogEndPR] = useState(false);
    const [subject, setSubject] = useState(DEFAULT_SUBJECT_FAILURE);
    const [subjectAlready, setSubjectAlready] = useState(DEFAULT_SUBJECT_ALREADY_FAILURE);
    const [selectedDateBegin, setSelectedDateBegin] = useState(new Date());
    const [selectedDateEnd, setSelectedDateEnd] = useState(new Date(selectedDateBegin.getTime() + 7*24*60*60*1000));

    const updateAllLists = () => {
        loadData('getUsersRequiredSoonPR?threshold=1638735823', updateRequiredSoon).then(r => {}); //TODO Сделать datepicker
        loadData('getAllDuringNowPrs', updateDuring).then(r => {});
    };

    useEffect(updateAllLists, [])

    const loadData = async (url, update) => {
        try {
            const res = await axios.get( appConfig.apiDomain + url)
            console.log(url);
            console.log(res.data);
            update(res.data);
        } catch (e) {
            console.error(e)
        }
    }

    const sendData = async (url, data, update) => {
        try {
            const res = await axios.post( appConfig.apiDomain + url, data)
            update(res.data);
        } catch (e) {
            console.error(e)
        }
    }

    const handleClickOpenDialogStartPR = () => {
        setOpenDialogStartPR(true);
    };

    const handleCloseDialogStartPR = () => {
        setOpenDialogStartPR(false);
    };

    const handleClickOpenDialogEndPR = () => {
        setOpenDialogEndPR(true);
    };

    const handleCloseDialogEndPR = () => {
        setOpenDialogEndPR(false);
    };

    const isTooLongAgo = (date) => {
        let difference = differenceInSeconds(new Date(), date*1000);
        console.log(difference)
        return difference > 31*24*60*60;
    }

    return (
        <div className="m-sm-30">
            <div className="mb-sm-30 flex justify-between">
                <div className="flex ">
                    <Breadcrumb
                        routeSegments={[
                            { name: 'Менеджер', path: '/manager' },
                            { name: 'Очередь PR', path: '/manager/queue' },
                        ]}
                    />
                </div>
                <Button variant={"outlined"} className={'flex'} startIcon={<Icon>refresh</Icon>} onClick={updateAllLists}>Обновить</Button>
            </div>
            <SimpleCard title="В ближайшее время требуется PR">
                <Grid container direction={"column"} spacing={3}>
                    { requiredSoon.length !== 0
                        ? requiredSoon.map((bg, ind) => (
                                <Grid item key={ind}>
                                    <div
                                        className={`h-145 w-100% border-radius-8 elevation-z6 ${ isTooLongAgo(bg.last_pr_date) ? 'bg-error' : 'bg-default' } flex justify-center items-center`}
                                    >
                                        <UserGrid userId={`${bg.user.id}`}
                                                  image={`${bg.user.image}`}
                                                  fullname={`${bg.user.fullname}`}
                                                  chipDesc={'Последний PR:'}
                                                  position={`${bg.user.states.map((value, index) => value.position.name).join(', ')}`}
                                                  lastPR={`${getTimeDifference(bg.last_pr_date*1000)} назад`}>
                                            <Button variant="outlined" onClick={() => {
                                                handleClickOpenDialogStartPR()
                                                setSubject(bg)
                                            }}> Запуск SR </Button>
                                            <Dialog
                                                open={openDialogStartPR}
                                                keepMounted
                                                onClose={handleCloseDialogStartPR}
                                                aria-describedby="alert-dialog-slide-description"
                                            >
                                                <DialogTitle>{"Уточните действие"}</DialogTitle>
                                                <DialogContent>
                                                    <DialogContentText id="alert-dialog-slide-description">
                                                        Вы действительно хотите начать SR для пользователя {`${subject.user.fullname}`}?
                                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                                            <Grid className='flex container justify-between'>
                                                                <Grid item className='flex'>
                                                                    <KeyboardDatePicker
                                                                        margin="normal"
                                                                        id="date-picker-dialog"
                                                                        label="Дата начала PR"
                                                                        format="dd/MM/yyyy"
                                                                        value={selectedDateBegin}
                                                                        onChange={setSelectedDateBegin}
                                                                        KeyboardButtonProps={{
                                                                            'aria-label': 'change date',
                                                                        }}
                                                                    />
                                                                </Grid>
                                                                <Grid item className='flex'>
                                                                    <KeyboardDatePicker
                                                                        margin="normal"
                                                                        id="date-picker-dialog"
                                                                        label="Дата окончания PR"
                                                                        format="dd/MM/yyyy"
                                                                        value={selectedDateEnd}
                                                                        onChange={setSelectedDateEnd}
                                                                        KeyboardButtonProps={{
                                                                            'aria-label': 'change date',
                                                                        }}
                                                                    />
                                                                </Grid>
                                                            </Grid>
                                                        </MuiPickersUtilsProvider>
                                                    </DialogContentText>
                                                </DialogContent>
                                                <DialogActions>
                                                    <Button variant="outlined" color='error' onClick={handleCloseDialogStartPR}>Отмена</Button>
                                                    {/*<NavLink to={`/pr/create/${subject.id}`} >*/}
                                                        <Button color="primary" onClick={() => {
                                                            sendData(`addDuringPr`, {
                                                                about_who_id: subject.user.id,
                                                            }, x => {}).then(x => {
                                                                updateAllLists();
                                                                setSubject(DEFAULT_SUBJECT_FAILURE);
                                                            });
                                                            handleCloseDialogStartPR();
                                                        }}>Да, запуск</Button>
                                                    {/*</NavLink>*/}
                                                </DialogActions>
                                            </Dialog>
                                        </UserGrid>
                                    </div>
                                </Grid>
                            ))
                        : <Grid item>
                            <div
                                className={`h-145 w-100% flex justify-center items-center`}
                            >
                                Нет PR, которые нужно провести в ближайшее время
                            </div>
                        </Grid>
                    }
                </Grid>
            </SimpleCard>
            <div className="py-3" />
            <SimpleCard title="Запущенные PR">
                <Grid container direction={"column"} spacing={3}>
                    { during.length !== 0
                        ? during.map((bg, ind) => (
                        <Grid item key={ind}>
                            <div
                                className={`h-145 w-100% border-radius-8 elevation-z6 bg-default flex justify-center items-center`}
                            >
                                <UserGrid userId={`${bg.about_who.id}`}
                                          image={`${bg.about_who.image}`}
                                          fullname={`${bg.about_who.fullname}`}
                                          position={`${bg.about_who.states.map((value, index) => value.position.name).join(', ')}`}
                                          chipDesc={'Статус:'}
                                          lastPR={`${TRANSLATE_STATUS[bg.status.name]}`}>
                                    <NavLink to={`/pr/view/${bg.id}`} ><Button variant="outlined" > Просмотр анкеты PR </Button></NavLink>
                                    <Button variant="contained" className='ml-4 bg-error' onClick={() => {
                                        handleClickOpenDialogEndPR()
                                        setSubjectAlready(bg)
                                    }}> Завершение PR </Button>
                                    <Dialog
                                        open={openDialogEndPR}
                                        keepMounted
                                        onClose={handleCloseDialogEndPR}
                                        aria-describedby="alert-dialog-slide-description"
                                    >
                                        <DialogTitle>{"Подтвердите действие"}</DialogTitle>
                                        <DialogContent>
                                            <DialogContentText id="alert-dialog-slide-description">
                                                Вы действительно хотите завершить PR для пользователя {`${subjectAlready.about_who.fullname}`}?
                                            </DialogContentText>
                                        </DialogContent>
                                        <DialogActions>
                                            <Button variant="outlined" color='error' onClick={handleCloseDialogEndPR}>Отмена</Button>
                                            {/*<NavLink to={`/pr/create/${subject.id}`} >*/}
                                            <Button color="primary" onClick={() => {
                                                sendData(`stopPR?id=${subjectAlready.about_who.id}`, x => {}).then(x => {
                                                    updateAllLists();
                                                    setSubjectAlready(DEFAULT_SUBJECT_ALREADY_FAILURE);
                                                });
                                                handleCloseDialogEndPR();
                                            }}>Да, завершить</Button>
                                            {/*</NavLink>*/}
                                        </DialogActions>
                                    </Dialog>
                                </UserGrid>
                            </div>
                        </Grid>
                    ))
                    : <Grid item>
                            <div
                                className={`h-145 w-100% flex justify-center items-center`}
                            >
                                В настоящее время нет запущенных PR
                            </div>
                        </Grid>
                    }
                </Grid>
            </SimpleCard>
        </div>
    )
}

export default Queue
