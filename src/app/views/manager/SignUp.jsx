import React, {useState, useEffect} from 'react'
import { Breadcrumb, SimpleCard } from 'app/components'
import {
    Button, Dialog, DialogActions, DialogContent, DialogTitle,
    Grid,
    Icon, Snackbar, TextField, Tooltip
} from '@material-ui/core'
import axios from "../../../axios";
import {appConfig} from "../../../config";
import {Alert, Autocomplete} from "@material-ui/lab";
import {TextValidator, ValidatorForm} from "react-material-ui-form-validator";
import useAuth from "../../hooks/useAuth";
import {authRoles} from "../../auth/authRoles";
import DialogContentText from "@material-ui/core/DialogContentText";

const SignUp = () => {


    const { user } = useAuth();

    const roles = [
        {
            id: 1,
            role_name: authRoles.employee[authRoles.employee - 1],
            role_view: 'Сотрудник',
        },
        {
            id: 2,
            role_name: authRoles.moderator[authRoles.moderator - 1],
            role_view: 'Модератор',
        },
        {
            id: 3,
            role_name: authRoles.depLeader[authRoles.depLeader - 1],
            role_view: 'Начальник отдела',
        },
        {
            id: 4,
            role_name: authRoles.admin[authRoles.admin - 1],
            role_view: 'Администратор системы',
        }
    ];
    const [state, setState] = useState({
        empRole: roles[0].id,
        empDepartment: user.states[0].department.id,
    })
    const [subject, setSubject] = useState({});
    const [departments, setDepartments] = useState([]);
    const [positions, setPositions] = useState([]);
    const [openDialog, setOpenDialog] = useState(false);



    const updateAllLists = () => {
        loadData('getPositions', setPositions).then(r => {});
        loadData('getDepartments', setDepartments).then(r => {});
    };

    useEffect(updateAllLists, [])
    useEffect(() => {
        ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
            // console.log(value)

            if (value !== state.password) {
                return false
            }
            return true
        })
        return () => ValidatorForm.removeValidationRule('isPasswordMatch')
    }, [state.password])

    const loadData = async (url, update) => {
        try {
            const res = await axios.get( appConfig.apiDomain + url)
            update(res.data);
        } catch (e) {
            console.error(e)
        }
    }

    const sendData = async (url, data, update) => {
        try {
            const res = await axios.post( appConfig.apiDomain + url, data)
            update(res.data);
        } catch (e) {
            console.error(e)
        }
    }

    const handleSubmit = (event) => {
        switchDisableSubmittion(true);
        console.log(resultValues);
        sendData('registerUser', resultValues, result => {
            if(result.code && result.code === 200) {
                setSubject({title: 'Поздравляем', text: `Пользователь ${fullname} успешно создан`});
                //setState({});
                window.location.reload();
            }
            else
                setSubject({title: 'Внимание', text: `Не удалось создать пользователя. Возникла непредвиденная ошибка.`});
            setOpenDialog(true);
        }).then(x => {
            switchDisableSubmittion(false);
        });
    }

    const handleChange = (event) => {
        event.persist()
        setState({
            ...state,
            [event.target.name]: event.target.value,
        })
    }

    const handleChangeAutofill = (name, value) => {
        setState({
            ...state,
            [name]: value,
        })
    }

    const {
        fullname,
        password,
        confirmPassword,
        email,
        empPosition,
        empDepartment,
        empRole,
    } = state;

    let resultValues = {
        'fullname': fullname,
        'password': password,
        'email': email,
        'role': empRole,
        'states': [
            {
                'department_id': empDepartment, //WARNING. ORDER MATTERS
                'position_id': empPosition,
                'grade_id': null,
            }
        ]
    }

    const [open, setOpen] = React.useState(false);
    const [openBad, setOpenBad] = React.useState(false);

    const handleCheckEmail = () => {
/*        loadData(`getUserByEmailPart?email=${email}`, users => {
            if(users.length == 0)
                setOpen(true);
            else
                setOpenBad(true);
        }).then(x => {});*/
        if(Math.random() > 0.5)
            setOpen(true);
        else
            setOpenBad(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
        setOpenBad(false);
    };

    const handleClickOpenDialog = () => {
        setOpenDialog(true);
    };

    const handleCloseDialog = () => {
        setOpenDialog(false);
    };

    const [disableSubmittion, switchDisableSubmittion] = useState(false);


    return (
        <div className="m-sm-30">
            <div className="mb-sm-30 flex justify-between">
                <div className="flex ">
                    <Breadcrumb
                        routeSegments={[
                            { name: 'Менеджер', path: '/manager' },
                            { name: 'Создание пользователя', path: '/manager/signup' },
                        ]}
                    />
                </div>
                {/*<Button variant={"outlined"} className={'flex'} startIcon={<Icon>refresh</Icon>} onClick={updateAllLists}>Обновить</Button>*/}
            </div>
            <SimpleCard title="Регистрация нового пользователя">
                <Grid container direction={"column"} spacing={3}>
                    <Grid item>
                        <div
                            className={`h-145 w-100% flex justify-center items-center`}
                        >
                            <ValidatorForm onSubmit={handleSubmit} onError={() => null}>
                                <Grid container className='flex justify-between'>
                                    <Grid item className='flex'>
                                        <TextValidator
                                            className="mb-4 w-300"
                                            label="Фамилия и имя"
                                            onChange={handleChange}
                                            type="text"
                                            name="fullname"
                                            value={fullname || ''}
                                            validators={['required']}
                                            errorMessages={[
                                                'это поле необходимо'
                                            ]}
                                        />
                                    </Grid>
                                    <Grid item className='flex'>
                                        <Tooltip className='m-4' title={"Укажите полное имя сотрудника"} fontSize="large" arrow>
                                            <Icon fontSize='small'>help_outline</Icon>
                                        </Tooltip>
                                    </Grid>
                                </Grid>
                                <Grid container className='flex justify-between'>
                                    <Grid item className='flex'>
                                        <TextValidator
                                            className="mb-4 w-300"
                                            label="Email"
                                            onChange={handleChange}
                                            type="email"
                                            name="email"
                                            value={email || ''}
                                            validators={['required', 'isEmail']}
                                            errorMessages={[
                                                'это поле необходимо',
                                                'email невалидный',
                                            ]}
                                        />
                                    </Grid>
                                    <Grid item className='flex'>
                                        <Tooltip  className='m-4' title={"Введите корпоративную или личную почту сотрудника. Она будет служить логином для входа, а также на неё придут данные для авторазиции"} fontSize="large" arrow>
                                            <Icon fontSize='small'>help_outline</Icon>
                                        </Tooltip>
                                    </Grid>
                                </Grid>
                                <Grid container className='flex justify-between'>
                                    <Grid item className='flex'>
                                        <TextValidator
                                            className="mb-4 w-300"
                                            label="Пароль"
                                            onChange={handleChange}
                                            name="password"
                                            type="password"
                                            value={password || ''}
                                            validators={[
                                                'required',
                                                'minStringLength:7'
                                            ]}
                                            errorMessages={[
                                                'это поле необходимо',
                                                'минимум 7 символов'
                                            ]}
                                        />
                                    </Grid>
                                    <Grid item className='flex'>
                                        <Tooltip className='m-4'  title={"Придумайте надёжный пароль для сотрудника. Минимальная длина пароля 7 символов"} fontSize="large" arrow>
                                            <Icon fontSize='small'>help_outline</Icon>
                                        </Tooltip>
                                    </Grid>
                                </Grid>
                                <Grid container className='flex justify-between'>
                                    <Grid item className='flex'>
                                        <TextValidator
                                            className="mb-4 w-300"
                                            label="Повторите пароль"
                                            onChange={handleChange}
                                            name="confirmPassword"
                                            type="password"
                                            value={confirmPassword || ''}
                                            validators={['required', 'isPasswordMatch']}
                                            errorMessages={[
                                                'это поле необходимо',
                                                "пароли не совпадают",
                                            ]}
                                        />
                                    </Grid>
                                    <Grid item className='flex'>
                                        <Tooltip className='m-4' title={"Пароли должны совпадать"} fontSize="large" arrow>
                                            <Icon fontSize='small'>help_outline</Icon>
                                        </Tooltip>
                                    </Grid>
                                </Grid>
                                { authRoles.admin.includes(user.role.name)
                                    ?
                                    <Grid container className='flex justify-between'>
                                        <Grid item className='flex'>
                                            <Autocomplete
                                                className="mb-4 w-300"
                                                options={departments}
                                                openOnFocus
                                                getOptionLabel={(option) => option.name}
                                                name='empDepartment'
                                                onChange={(event, value) => {
                                                    if(value !== null) {
                                                        handleChangeAutofill('empDepartment', value.id);
                                                    }
                                                }}
                                                renderInput={(params) => (
                                                    <TextField
                                                        {...params}
                                                        label="Выберите подразделение"
                                                        variant="standard"
                                                        value={empDepartment || ''}
                                                        fullWidths
                                                    />
                                                )}
                                            />
                                        </Grid>
                                        <Grid item className='flex'>
                                            <Tooltip className='m-4'  title={"Поскольку вы являетесь администратором системы, вы можете выбрать любое подразделение"} fontSize="large" arrow>
                                                <Icon fontSize='small'>help_outline</Icon>
                                            </Tooltip>
                                        </Grid>
                                    </Grid> : <div/>
                                }
                                <Grid container className='flex justify-between'>
                                    <Grid item className='flex'>
                                        <Autocomplete
                                            className="mb-4 w-300"
                                            options={positions}
                                            openOnFocus
                                            getOptionLabel={(option) => option.name}
                                            name='empPosition' //Autofill elements doesn't have a name property
                                            onChange={(event, value) => {
                                                handleChangeAutofill('empPosition', value.id);

                                            }}
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    label="Выберите должность"
                                                    variant="standard"
                                                    value={empPosition || ''}
                                                    fullWidth
                                                    required
                                                />
                                            )}
                                        />
                                    </Grid>
                                    <Grid item className='flex'>
                                        <Tooltip className='m-4'  title={"Выберите должность добавляемого сотрудника"} fontSize="large" arrow>
                                            <Icon fontSize='small'>help_outline</Icon>
                                        </Tooltip>
                                    </Grid>
                                </Grid>
                                { authRoles.depLeader.includes(user.role.name)
                                    ?
                                    <Grid container className='flex justify-between'>
                                        <Grid item className='flex'>
                                            <Autocomplete
                                                className="mb-4 w-300"
                                                options={!authRoles.admin.includes(user.role.name) ? roles.slice(0, 2) : roles}
                                                openOnFocus
                                                getOptionLabel={(option) => option.role_view}
                                                name='empRole'
                                                onChange={(event, value) => {
                                                    if(value !== null)
                                                        handleChangeAutofill('empRole', value.id);
                                                }}
                                                defaultValue={roles[0]}
                                                renderInput={(params) => (
                                                    <TextField
                                                        {...params}
                                                        label="Выберите роль в системе"
                                                        variant="standard"
                                                        required
                                                        value={empRole || ''}
                                                        fullWidths
                                                    />
                                                )}
                                            />
                                        </Grid>
                                        <Grid item className='flex'>
                                            <Tooltip className='m-4'  title={"Поскольку вы являетесь начальником отдела или администратором системы, вы можете выбрать роль сотрудника. Обратите внимание! Если вы администратор, вы можете создавать других администраторов! Будьте осторожны с этой возможностью! Это действие эквивалентно с выдачей root-прав и может повлечь за собой необратимые последствия. Удостоверьтесь, что вы доверяете целевому субъекту!"} fontSize="large" arrow>
                                                <Icon fontSize='small'>help_outline</Icon>
                                            </Tooltip>
                                        </Grid>
                                    </Grid> : <div/>
                                }
                                <Button color="primary" variant="contained" type="submit" className='mr-4' disabled={disableSubmittion}>
                                    <Icon>send</Icon>
                                    <span className="pl-2 capitalize">Регистрация</span>
                                </Button>
                                <Button color='primary' variant='outlined' onClick={handleCheckEmail}>
                                    <span className='pl-2 capitalize'>Проверить e-mail</span>
                                </Button>
                                <Snackbar open={open} autoHideDuration={6000} onClose={handleClose} anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}>
                                    <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                                        Этот e-mail доступен
                                    </Alert>
                                </Snackbar>
                                <Snackbar open={openBad} autoHideDuration={6000} onClose={handleClose} anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}>
                                    <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
                                        Этот e-mail недоступен
                                    </Alert>
                                </Snackbar>
                                <Dialog
                                    open={openDialog}
                                    keepMounted
                                    onClose={handleCloseDialog}
                                    aria-describedby="alert-dialog-slide-description"
                                >
                                    <DialogTitle>{`${subject.title}`}</DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-slide-description">
                                            {`${subject.text}`}
                                        </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button color="primary" onClick={() => {
                                            setSubject({});
                                            handleCloseDialog();
                                        }}>Ок</Button>
                                    </DialogActions>
                                </Dialog>
                            </ValidatorForm>
                        </div>
                    </Grid>
                </Grid>
            </SimpleCard>
        </div>
    )
}

export default SignUp
