import React from 'react'
import {authRoles} from "../../auth/authRoles";

const managerRoutes = [
    {
        path: '/manager/queue',
        component: React.lazy(() => import('./Queue')),
        auth: authRoles.moderator,
    },
    {
        path: '/manager/signup',
        component: React.lazy(() => import('./SignUp')),
        auth: authRoles.moderator,
    },
]

export default managerRoutes
