import React, { useState, useEffect} from 'react'
import { Breadcrumb, SimpleCard } from 'app/components'
import {Avatar, Box, Button, Grid, Icon, Tooltip} from '@material-ui/core'
import axios from "../../../axios";
import {appConfig} from "../../../config";
import {Redirect, useParams} from "react-router-dom";
import useAuth from "../../hooks/useAuth";
import LittleUserGrid from '../../components/UserGrid/LittleUserGrid'
import {TextValidator, ValidatorForm} from "react-material-ui-form-validator";
import TwoListDnD from "./TwoListDnD";
import Chip from "@material-ui/core/Chip";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import AsyncAutocomplete from "../../components/AsyncAutoComplete/AsyncAutocomplete";


const AddUp = () => {

    const { during_pr_id } = useParams();
    const { user } = useAuth();

    const [availableRespondents, setAvailableRespondents] = useState([]);
    const [selectedRespondents, setSelectedRespondents] = useState([]);
    const [disableSubmittion, setDisableSubmittion] = useState(false);
    const [competences, updateCompetences] = useState([]);

    const [state, setState] = useState({
        selectedRespondents: [],
    }); //состояния вообще всех полей на странице

    const categories = [
        { id: 1, label: 'Пир' },
        { id: 2, label: 'Менеджер' },
        { id: 3, label: 'Подчинённый' },
        { id: 4, label: 'Стейкхолдер' },
    ]

    useEffect(() => {
        loadData('getUsersByDepartment', users => {
            for (let one_user of users) {
                if(one_user.id === user.id)
                    continue;
                one_user = {...one_user, ['chosenCategory']: {boxed: categories[0].label}};
                let newElement = {id: one_user.id, content: <LittleUserGrid
                        image={one_user.avatar}
                        fullname={one_user.fullname}
                        categories={categories}
                        userId={one_user.id}
                        email={one_user.email}
                        userCategory={one_user.chosenCategory}
                    />};
                console.log(newElement);
                setAvailableRespondents(old => [...old, newElement])
            }

        })
        updateCompetences(getDefaultCompetencies())
        updateSelectedCompetences(getDefaultCompetencies())
        loadData('', x => {});
    }, [])

    function getDefaultCompetencies(){
        let result = []
        user.states.map((val, ind) => {
            result = [...result, ...val.position.competencies];
        });
        return result;
    }


    const loadData = async (url, update) => {
        try {
            const res = await axios.get( appConfig.apiDomain + url)
            update(res.data);
        } catch (e) {
            console.error(e)
        }
    }

    const sendData = async (url, data, update) => {
        try {
            const res = await axios.post( appConfig.apiDomain + url, data)
            update(res.data, res);
        } catch (e) {
            console.error(e)
        }
    }

    function resultValues() {
        let textRepr = `Что делали:\n${whatDid}\n\nЧто было хорошо?\n${whatWasGood}\n\nЧто было плохо?\n${whatWasBad}`;
        let responds = []
        for (let selectedRespondent of selectedRespondents) {
            responds.push({
                who_wrote_id: selectedRespondent.id,
                respondent_group_id: selectedRespondent.content.props.userCategory.boxed.id
            });
        }
        const competencesIds = []
        for(let cid of selectedCompetences) {
            competencesIds.push(competences.map((val, ind) => val.name).indexOf(cid));
        }
        return {
            competencies: competencesIds,
            text: textRepr,
            reviewers: responds,
        }
    }

    const handleSubmit = (event) => {
        setDisableSubmittion(true);
        console.log(resultValues());
        sendData('addReviews', resultValues(), (result, reps) => {
            console.log(reps);
            if(reps.status && reps.status === 200) {
                // setState({});
                document.location.href = "/";
            }
        }).then(x => {
            setDisableSubmittion(false)
        });
    }

    const handleChange = (event) => {
        event.persist()
        setState({
            ...state,
            [event.target.name]: event.target.value,
        })
    }

    const [foundUsers, updateFoundUsers] = React.useState([]); //TODO Возможно нужно использовать при поиске юзеров для потенциальных

    const handleSearch = (event) => {
        event.persist()
        loadData("findUserByFirstname?name=" + event.target.value, updateFoundUsers);
    }

    const [selectedCompetences, updateSelectedCompetences] = React.useState([]);

    const handleUpdateSelectedCompetences = (e, v) => {
        updateSelectedCompetences(v);
    };

    const {
        rolesAndExpectations,
        whatDid,
        whatWasGood,
        whatWasBad,
        SOMEFIELD,
    } = state;

    return (
        <div className="m-sm-30">
            <div className="mb-sm-30 flex justify-between">
                <div className='flex'>
                    <Breadcrumb
                        routeSegments={[
                            { name: 'PR', path: '/pr' },
                            { name: 'Заполнение SR', path: `/pr/addup/${during_pr_id}` },
                        ]}
                    />
                </div>
                <div className='flex'>

                </div>
            </div>
            <SimpleCard title="Заполнение SR">
                <div className='flex justify-between'>
                    <Grid className='flex' container direction={"column"} spacing={3}>
                        <Grid item>
                            <div
                                className={`h-145 w-100% flex justify-center items-center`}
                            >
                                <ValidatorForm onSubmit={handleSubmit} onError={() => null}>
                                    <Grid container className='flex column'>
                                        <Grid item  className='flex column'>
                                            <Box sx={{flexDirection: 'column'}}>
                                                <Grid item container className='flex justify-between'>
                                                    <Grid item className='flex'>
                                                        <Autocomplete
                                                            multiple
                                                            id="tags-filled"
                                                            options={competences.map((option) => option.name)}
                                                            freeSolo
                                                            onChange={handleUpdateSelectedCompetences}
                                                            name="rolesAndExpectations"
                                                            renderTags={(value, getTagProps) =>
                                                                value.map((option, index) => (
                                                                    <Chip
                                                                        variant="outlined"
                                                                        label={option}
                                                                        {...getTagProps({ index })}
                                                                    />
                                                                ))
                                                            }
                                                            renderInput={(params) => (
                                                                <TextValidator
                                                                    {...params}
                                                                    className="mb-4 w-300"
                                                                    label="Роли и ожидания"
                                                                    placeholder="Укажите несколько"
                                                                    onChange={handleChange}
                                                                    value={rolesAndExpectations || ''}
                                                                    /*validators={['required']}*/
                                                                    errorMessages={[
                                                                        'это поле необходимо'
                                                                    ]}
                                                                    variant='outlined'
                                                                />
                                                            )}
                                                        />
                                                    </Grid>
                                                    <Grid item className='flex'>
                                                        <Tooltip className='m-4' title={"Включает в себя описание ожиданий от ваших позиций и ролей. Здесь может быть информация о функциональных ожиданиях, описание роли наставника, спикера, участника образовательной программы, —  любая позиция и роль, в которой вы выступаете."} fontSize="large" arrow>
                                                            <Icon fontSize='small'>help_outline</Icon>
                                                        </Tooltip>
                                                    </Grid>
                                                </Grid>
                                                <Grid item container className='flex justify-between'>
                                                    <Grid item className='flex'>
                                                        <TextValidator
                                                            className="mb-4 w-300"
                                                            label="Что делал"
                                                            onChange={handleChange}
                                                            type="text"
                                                            name="whatDid"
                                                            multiline
                                                            rows={4}
                                                            variant='outlined'
                                                            value={whatDid || ''}
                                                            validators={['required']}
                                                            errorMessages={[
                                                                'это поле необходимо'
                                                            ]}
                                                        />
                                                    </Grid>
                                                    <Grid item className='flex'>
                                                        <Tooltip className='m-4' title={"Краткие результаты последнего периода оценки. Это ключевая секция, в которой вы перечисляете все проекты и деятельность, которой занимались. Это могут быть цели вашей команды, на которые вы фактически повлияли, проекты, над которыми вы работали, детали выполнения ваших базовых функциональных обязанностей. Здесь же стоит упомянуть про свои планы по росту и развитию и подчеркнуть, что вы делали, чтобы достичь желаемого. Всякие дополнительные активности вроде выступления на митапах или участия в хакатонах тоже добавляются сюда. Старайтесь не лить воды, а говорить только о фактах."} fontSize="large" arrow>
                                                            <Icon fontSize='small'>help_outline</Icon>
                                                        </Tooltip>
                                                    </Grid>
                                                </Grid>
                                                <Grid item container className='flex justify-between'>
                                                    <Grid item className='flex'>
                                                        <TextValidator
                                                            className="mb-4 w-300"
                                                            label="Что было хорошо?"
                                                            onChange={handleChange}
                                                            type="text"
                                                            name="whatWasGood"
                                                            multiline
                                                            rows={4}
                                                            variant='outlined'
                                                            value={whatWasGood || ''}
                                                            validators={['required']}
                                                            errorMessages={[
                                                                'это поле необходимо'
                                                            ]}
                                                        />
                                                    </Grid>
                                                    <Grid item className='flex'>
                                                        <Tooltip className='m-4' title={"Здесь вы перечисляете те факты, которые считаете своими основными достижениями и успехами. В основном делайте упор на рабочие достижения — успешно запущенные проекты, налаженные с кем-то взаимоотношения, успешно выпущенные обученные новички. Опять же, не будьте многословными и старайтесь удерживать фокус на главном."} fontSize="large" arrow>
                                                            <Icon fontSize='small'>help_outline</Icon>
                                                        </Tooltip>
                                                    </Grid>
                                                </Grid>
                                                <Grid item container className='flex justify-between'>
                                                    <Grid item className='flex'>
                                                        <TextValidator
                                                            className="mb-4 w-300"
                                                            label="Что было плохо?"
                                                            onChange={handleChange}
                                                            type="text"
                                                            name="whatWasBad"
                                                            multiline
                                                            rows={4}
                                                            variant='outlined'
                                                            value={whatWasBad || ''}
                                                            validators={['required']}
                                                            errorMessages={[
                                                                'это поле необходимо'
                                                            ]}
                                                        />
                                                    </Grid>
                                                    <Grid item className='flex'>
                                                        <Tooltip className='m-4' title={"Максимально открыто перечислите свои основные провалы, то, чем вы сами недовольны и что хотели бы исправить и улучшить в будущем. Сюда можно включать как провалы конкретных поставленных перед вами или командой целей, так и свои личные ошибки."} fontSize="large" arrow>
                                                            <Icon fontSize='small'>help_outline</Icon>
                                                        </Tooltip>
                                                    </Grid>
                                                </Grid>
                                                <Button color="primary" variant="contained" type="submit" className='mr-4' disabled={disableSubmittion}>
                                                    <Icon>send</Icon>
                                                    <span className="pl-2 capitalize">Заполнить</span>
                                                </Button>
                                            </Box>
                                        </Grid>
                                        <Grid item className='flex column'>
                                            <TwoListDnD
                                                firstList={selectedRespondents}
                                                setFirstList = {setSelectedRespondents}
                                                secondList={availableRespondents}
                                                setSecondList={setAvailableRespondents}
                                                firstListLabel={<div>Выбранные респонденты</div>}
                                                secondListLabel={<div className='w-max'>
                                                    Возможные респонденты
                                                    <br/>
                                                    {/*<AsyncAutocomplete onChange={handleSearch} options={foundUsers}/>*/}
                                                    <TextField onChange={handleSearch} variant='outlined' className='w-400 m-3' label='Добавить возможных респондентов'/> {/*TODO сделать автодобавление при "поиске"*/}
                                                </div>}
                                            />
                                        </Grid>
                                    </Grid>

                                </ValidatorForm>

                                {/*
                                <br/>
                                <Button onClick={() => {
                                    alert(selected)
                                }}>Check</Button>
*/}
                            </div>
                        </Grid>
                    </Grid>
                </div>
            </SimpleCard>
        </div>
    )
}

export default AddUp
