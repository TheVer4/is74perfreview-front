import React, { useState, useEffect} from 'react'
import { Breadcrumb, SimpleCard } from 'app/components'
import {Avatar as MuiAvatar, Box, Button, Grid, Icon, Tooltip, Typography} from '@material-ui/core'
import axios from "../../../axios";
import {appConfig} from "../../../config";
import {useParams} from "react-router-dom";
import useAuth from "../../hooks/useAuth";
// import LittleUserGrid from '../../components/UserGrid/LittleUserGrid'
import {TextValidator, ValidatorForm} from "react-material-ui-form-validator";
import TwoListDnD from "./TwoListDnD";
import Chip from "@material-ui/core/Chip";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import styled from "styled-components";
import {Rating} from "@material-ui/lab";
import {getTimeDifference} from "../../../utils";

const Avatar = styled(MuiAvatar)({
    width: 132,
    height: 132
});

const Card = styled('div')({
    width: '100%',
    margin: '15px;'
});

const labels = {
    0: 'Затрудняюсь ответить',
    1: 'Значительно ниже ожиданий',
    2: 'Ниже ожиданий',
    3: 'Соответствует ожиданиям',
    4: 'Выше ожиданий',
    5: 'Значительно выше ожиданий',
};

function NewlineText(props) {
    const text = props.text + '';

    function getMargin(id) {
        return id % 2 === 0 ? 'm-0' : 'mb-3 mt-0 ml-0 mr-0';
    }

    let result =  text.split('\n').map((str, id) => <p className={getMargin(id)}>{str}</p>);
    return result;
}

const LeaveReview = () => {

    const { during_pr_id } = useParams();
    const { user } = useAuth();

    const [markValue, setMarkValue] = React.useState(0);
    const [markHover, setMarkHover] = React.useState(-1);
    const [duringPrObject, updateDuringPrObject] = React.useState(null);
    const [selfReviewData, updateSelfReviewData] = React.useState({
        during_pr: {
            about_who: {
                fullname: null
            },
            competencies: [],
        },
        date: null
    });

    const [disableSubmittion, setDisableSubmittion] = useState(false);
    const [competences, updateCompetences] = useState([
        {name: "Целеустремлённость"},
        {name: "Пунктуальность"},
    ]);

    const [state, setState] = useState({
        selectedRespondents: [],
    });

    useEffect(() => {
        loadData(`getDuringPr?id=${during_pr_id}`, (result) => {
            loadData(`getReview?user_id=${result.about_who.id}&during_pr_id=${during_pr_id}`, (result2) => {
                updateSelfReviewData(result2);
                updateCompetences(result2.during_pr.competencies);
            })
            console.log(selfReviewData)
        });
        // loadData('getUsersByDepartment', users => {
        //     for (let one_user of users) {
        //         if(one_user.id === user.id)
        //             continue
        //         console.log(one_user);
        //         let newElement = {id: one_user.id, content: <LittleUserGrid image={one_user.avatar} fullname={one_user.fullname} position={one_user.position} department={one_user.department} userId={one_user.id} email={one_user.email}/>};
        //         console.log(newElement);
        //         setAvailableRespondents(old => [...old, newElement])
        //     }
        //
        // })
    }, [])

    const loadData = async (url, update) => {
        try {
            const res = await axios.get( appConfig.apiDomain + url)
            update(res.data);
        } catch (e) {
            console.error(e)
        }
    }

    const sendData = async (url, data, update) => {
        try {
            const res = await axios.post( appConfig.apiDomain + url, data)
            update(res.data, res);
        } catch (e) {
            console.error(e)
        }
    }

    function resultValues() {
        let textRepr = `Комментарий к оценке:\n${markNote}\n\nЧто начать делать?\n${whatToStartDoing}\n\nЧто перестать делать?\n${whatToStopDoing}`;
        return {
            text: textRepr,
            during_pr_id: during_pr_id,
            relevantCompetences: selectedCompetences,
            mark: markValue,
        }
    }

    const handleSubmit = (event) => {
        setDisableSubmittion(true);
        loadData(`getReview?user_id=${user.id}&during_pr_id=${during_pr_id}`, (resultReview) => {
            sendData(`updateReview?id=${resultReview.id}`, resultValues(), (result, reps) => {
                console.log(reps);
                if(reps.status && reps.status === 200) {
                    // setState({});
                    document.location.href = `/pr/view/${during_pr_id}`;
                }
            }).then(x => {
                setDisableSubmittion(false)
            });
        })

    }

    const handleChange = (event) => {
        event.persist()
        setState({
            ...state,
            [event.target.name]: event.target.value,
        })
    }

    const [selectedCompetences, updateSelectedCompetences] = React.useState([]);

    const {
        rolesAndExpectations,
        markNote,
        whatToStartDoing,
        whatToStopDoing,
        SOMEFIELD,
    } = state;

    const handleUpdateSelectedCompetences = (e, v) => {
        updateSelectedCompetences(v);
    };

    return (
        <div className="m-sm-30">
            <div className="mb-sm-30 flex justify-between">
                <div className='flex'>
                    <Breadcrumb
                        routeSegments={[
                            { name: 'PR', path: '/pr' },
                            { name: 'Заполнение PR', path: `/pr/leaveReview/${during_pr_id}` },
                        ]}
                    />
                </div>
                <div className='flex'>

                </div>
            </div>
            <SimpleCard title="Отзыв ревьюируемого">
                <div className='flex justify-between'>
                    <Grid className='flex' container direction={"column"} spacing={3}>
                        <Grid item>
                            <div
                                className={`h-145 w-100% flex justify-center items-center`}
                            >
                                <Card>
                                    <Grid container spacing={2}>
                                    <Grid item direction={"column"} justify={"center"}>
                                        <Avatar src={'props.image'}/>
                                    </Grid>
                                    <Grid item xs={12} sm container>
                                        <Grid item xs container direction="column" spacing={2}>
                                            <Grid item xs>
                                                <Typography gutterBottom variant="h5" component="div">
                                                    {selfReviewData.during_pr.about_who.fullname}
                                                </Typography>
                                                <Typography variant="body2" gutterBottom>
                                                    <NewlineText text={selfReviewData.text}/>
                                                </Typography>
                                                <Typography variant="body2" color="text.secondary">
                                                </Typography>
                                            </Grid>
                                            <Grid item>
                                                <Typography sx={{ cursor: 'pointer' }} variant="body2">
                                                    Роли и ожидания:
                                                    {
                                                        competences.map((val, ind) =>
                                                            <Chip className={'mx-1'} label={val.name}/>
                                                        )
                                                    }
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="subtitle1" component="div">
                                                Дата: <Chip label={getTimeDifference(selfReviewData.date*1000) + ' назад'}/>
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                </Card>
                            </div>
                        </Grid>
                    </Grid>
                </div>
            </SimpleCard>
            <div className="py-3" />
            <SimpleCard title="Заполнение PR">
                <div className='flex justify-between'>
                    <Grid className='flex' container direction={"column"} spacing={3}>
                        <Grid item>
                            <div
                                className={`h-145 w-100% flex justify-center items-center`}
                            >
                                <ValidatorForm onSubmit={handleSubmit} onError={() => null}>
                                    <Typography component="legend">Общая оценка</Typography>
                                    <Rating
                                        precision={1}
                                        onChange={(event, newValue) => {
                                            setMarkValue(newValue);
                                        }}
                                        onChangeActive={(event, newHover) => {
                                            setMarkHover(newHover);
                                        }}
                                        size="large"
                                    />
                                    {markValue !== null && (
                                        <Box sx={{ ml: 2 }}>{labels[markHover !== -1 ? markHover : markValue]}</Box>
                                    )}
                                    <br/>
                                    <TextValidator
                                        className="mb-4 w-300"
                                        label="Комментарий к оценке"
                                        onChange={handleChange}
                                        type="text"
                                        name="markNote"
                                        multiline
                                        rows={4}
                                        variant='outlined'
                                        value={markNote || ''}
                                        validators={['required']}
                                        errorMessages={[
                                            'это поле необходимо'
                                        ]}
                                    />
                                    <TextValidator
                                        className="mb-4 w-300"
                                        label="Что начать делать"
                                        onChange={handleChange}
                                        type="text"
                                        name="whatToStartDoing"
                                        multiline
                                        rows={4}
                                        variant='outlined'
                                        value={whatToStartDoing || ''}
                                        validators={['required']}
                                        errorMessages={[
                                            'это поле необходимо'
                                        ]}
                                    />
                                    <TextValidator
                                        className="mb-4 w-300"
                                        label="Что перестать делать"
                                        onChange={handleChange}
                                        type="text"
                                        name="whatToStopDoing"
                                        multiline
                                        rows={4}
                                        variant='outlined'
                                        value={whatToStopDoing || ''}
                                        validators={['required']}
                                        errorMessages={[
                                            'это поле необходимо'
                                        ]}
                                    />
                                    <br/>
                                    <Autocomplete
                                        multiple
                                        id="tags-filled"
                                        options={competences.map((option) => option.name)}
                                        freeSolo
                                        onChange={handleUpdateSelectedCompetences}
                                        name="rolesAndExpectations"
                                        renderTags={(value, getTagProps) =>
                                            value.map((option, index) => (
                                                <Chip
                                                    variant="outlined"
                                                    label={option}
                                                    {...getTagProps({ index })}
                                                />
                                            ))
                                        }
                                        renderInput={(params) => (
                                            <TextValidator
                                                {...params}
                                                className="mb-4 w-300"
                                                label="Релевантные роли и ожидания"
                                                placeholder="Укажите несколько"
                                                onChange={handleChange}
                                                value={rolesAndExpectations || ''}
                                                /*validators={['required']}*/
                                                errorMessages={[
                                                    'это поле необходимо'
                                                ]}
                                                variant='outlined'
                                            />
                                        )}
                                    />
                                    <Button color="primary" variant="contained" type="submit" className='mr-4' disabled={disableSubmittion}>
                                        <Icon>send</Icon>
                                        <span className="pl-2 capitalize">Заполнить</span>
                                    </Button>
                                </ValidatorForm>

                                {/*
                                <br/>
                                <Button onClick={() => {
                                    alert(selected)
                                }}>Check</Button>
                                */}
                            </div>
                        </Grid>
                    </Grid>
                </div>
            </SimpleCard>
        </div>
    )
}

export default LeaveReview
