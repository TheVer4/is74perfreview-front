import React, {useEffect, useState} from 'react'
import {Breadcrumb, SimpleCard} from 'app/components'
import {Avatar, Grid, Typography} from '@material-ui/core'
import axios from "../../../axios";
import {appConfig} from "../../../config";
import {useParams} from "react-router-dom";
import {
    AvatarGroup,
    Rating,
    Skeleton,
    Timeline,
    TimelineConnector,
    TimelineDot,
    TimelineItem,
    TimelineOppositeContent,
    TimelineSeparator
} from "@material-ui/lab";
import ReactEcharts from "echarts-for-react";
import {useTheme} from "@material-ui/core/styles";
import UserCommentGrid from "../../components/UserGrid/UserCommentGrid";
import {getTimeDifference} from "../../../utils";
import * as PropTypes from "prop-types";

function NewlineText(props) {
    const text = props.text + '';

    function getMargin(id) {
        return id % 2 === 0 ? 'm-0' : 'mb-3 mt-0 ml-0 mr-0';
    }

    let result =  text.split('\n').map((str, id) => <p className={getMargin(id)}>{str}</p>);
    return result;
}

NewlineText.propTypes = {text: PropTypes.any};
const View = () => {

    const theme = useTheme()

    const option = {
        grid: {
            top: '10%',
            bottom: '10%',
            left: '5%',
            right: '5%',
        },
        legend: {
            itemGap: 20,
            icon: 'circle',
            textStyle: {
                color: theme.palette.text.secondary,
                fontSize: 13,
                fontFamily: 'roboto',
            },
        },
        tooltip: {},
        xAxis: {
            type: 'category',
            data: ['Работа в команде', 'Системное мышление', 'Управлеине процессами', 'Осознанность', 'Клиентоориентированность', 'Коммуникация', 'Пунктуальность'],
            axisLine: {
                show: false,
            },
            axisTick: {
                show: false,
            },
            axisLabel: {
                color: theme.palette.text.secondary,
                fontSize: 14,
                fontFamily: 'roboto',
            },
        },
        yAxis: {
            type: 'value',
            axisLine: {
                show: false,
            },
            axisTick: {
                show: false,
            },
            splitLine: {
                //show: false,
                lineStyle: {
                    color: theme.palette.text.secondary,
                    opacity: 0.35,
                },
            },
            axisLabel: {
                color: theme.palette.text.secondary,
                fontSize: 13,
                fontFamily: 'roboto',
            },
        },
        series: [
            {
                data: [30, 40, 20, 50, 40, 80, 90],
                type: 'line',
                stack: 'Оценка ревьюируемого',
                name: 'Оценка ревьюируемого',
                smooth: true,
                symbolSize: 4,
                lineStyle: {
                    width: 4,
                },
            },
            {
                data: [20, 50, 15, 50, 30, 70, 95],
                type: 'line',
                stack: 'Оценка менеджера',
                name: 'Оценка менеджера',
                smooth: true,
                symbolSize: 4,
                lineStyle: {
                    width: 4,
                },
            },
        ],
    }

    const { during_pr_id } = useParams();
    /*let [prLifeCycle, updatePrLifeCycle] = useState([
        {
            bgClass: 'bg-primary',
            text: 'Я крутой сотрудник',
            mark: 5.0
        },
        {
            bgClass: 'bg-secondary',
            text: 'Умеренно крутой сотрудник',
            mark: 4.0
        },
        {
            bgClass: 'bg-primary',
            text: 'Вынужден согласиться с мнением менеджера',
            mark: 4.5
        },
        {
            bgClass: 'bg-light-green',
            text: 'Отстойный чел, ненавижу его',
            mark: 2.0
        },
        {
            bgClass: 'bg-light-green',
            text: 'Я, чёрт возьми, в своём познании настолько преисполнился, что я как будто бы уже 100 триллионов миллиардов лет, как бы, проживаю на триллионах и триллионах таких же планет, понимаешь? Как эта Земля. Мне уже этот мир абсолютно понятен, и я здесь ищу только одного: покоя, умиротворения и вот этой гармонии от слияния с бесконечно вечным.',
            mark: 3.5
        },
    ]);*/
    /*let [tookPart, updateTookPart] = useState([
        {
            fullname: 'Remy Sharp',
            image: '/assets/images/face-1.jpg',
        },
        {
            fullname: 'Travis Howard',
            image: '/assets/images/face-2.jpg',
        },
        {
            fullname: 'Remy Sharp',
            image: '/assets/images/face-1.jpg',
        },
        {
            fullname: 'Cindy Baker',
            image: '/assets/images/face-3.jpg',
        },
        {
            fullname: 'Agnes Walker',
            image: '/assets/images/face-4.jpg',
        },
        {
            fullname: 'Trevor Henderson',
            image: '/assets/images/face-5.jpg',
        },
    ])*/
    let [reviewDetails, updateReviewDetails] = useState([
        {
            id: <Skeleton/>,
            text: <Skeleton/>,
            mark: null,
            date: <Skeleton/>,
            fullname: <Skeleton/>,
            avatar: 'loading',
        },
        {
            id: <Skeleton/>,
            text: <Skeleton/>,
            mark: null,
            date: <Skeleton/>,
            fullname: <Skeleton/>,
            avatar: 'loading',
        }
    ]);
    const [subject, setSubject] = React.useState(-1);

    useEffect(() => {
        loadData(`getReviews?id=${during_pr_id}`, updateReviewDetails).then(r => {});
    }, [])

    const loadData = async (url, update) => {
        try {
            const res = await axios.get( appConfig.apiDomain + url)
            update(res.data);
        } catch (e) {
            console.error(e)
        }
    }

    return (
        <div className="m-sm-30">
            <div className="mb-sm-30 flex justify-between">
                <div className='flex'>
                    <Breadcrumb
                        routeSegments={[
                            { name: 'PR', path: '/PR' },
                            { name: 'Просмотр', path: `/PR/view/${during_pr_id}` },
                        ]}
                    />
                </div>
                <div className='flex'>
                    <div className='flex justify-end'>
                        <Grid spacing={2} className='flex' sx={{ alignItems: 'center', spacing: 2 }}>
                            <Grid item container direction={"column"} className='flex column'>
                                <div className='flex justify-center'><div/><Typography variant='h5' className='flex item'>86%</Typography><div/></div>
                                <Typography variant='caption' className='flex item' >Процент завершения</Typography>
                            </Grid>
                            <Grid item>
                                <AvatarGroup max={4}>
                                    {
                                        reviewDetails.map((user, ind) => (
                                            user.date != null ? <Avatar alt={user.fullname} src={user.avatar} /> : <div/>
                                        ))
                                    }
                                </AvatarGroup>
                            </Grid>
                        </Grid>
                    </div>
                </div>
            </div>
            <SimpleCard title="История развития PR пользователя">
                <div className='flex justify-between'>
                    <Grid className='flex' container direction={"column"} spacing={3}>
                        {reviewDetails.map((detail, ind) => (
                            detail.date != null ?
                                <Grid item key={ind}>
                                    <div
                                        className={`h-145 w-100% border-radius-8 elevation-z6 bg-light-primary flex justify-center items-center`}
                                    >
                                        <UserCommentGrid
                                            image={detail.avatar}
                                            fullname={detail.fullname}
                                            date={getTimeDifference(detail.date*1000) + ' назад'}
                                            mark={detail.mark}>
                                            <NewlineText text={detail.text}/>
                                        </UserCommentGrid>
                                    </div>
                                </Grid>:
                                <div/>

                        ))}
                    </Grid>
                    <div className='flex'>
                        <Timeline>
                            <h4 className='flex'>Status</h4>
                            <TimelineItem>
                                <TimelineSeparator>
                                    <TimelineDot variant='outlined' color="primary"/>
                                    <TimelineConnector/>
                                </TimelineSeparator>
                                <TimelineOppositeContent>Opened</TimelineOppositeContent>
                            </TimelineItem>
                            <TimelineItem>
                                <TimelineSeparator>
                                    <TimelineDot variant='default' color="primary"/>
                                    <TimelineConnector/>
                                </TimelineSeparator>
                                <TimelineOppositeContent>Self Review</TimelineOppositeContent>
                            </TimelineItem>
                            <TimelineItem>
                                <TimelineSeparator>
                                    <TimelineDot variant='outlined' color="primary"/>
                                    <TimelineConnector/>
                                </TimelineSeparator>
                                <TimelineOppositeContent>Manager</TimelineOppositeContent>
                            </TimelineItem>
                            <TimelineItem>
                                <TimelineSeparator>
                                    <TimelineDot variant='outlined' color="primary"/>
                                    <TimelineConnector/>
                                </TimelineSeparator>
                                <TimelineOppositeContent>Team Review</TimelineOppositeContent>
                            </TimelineItem>
                            <TimelineItem>
                                <TimelineSeparator>
                                    <TimelineDot variant='outlined' color="primary"/>
                                    <TimelineConnector/>
                                </TimelineSeparator>
                                <TimelineOppositeContent>Final Review</TimelineOppositeContent>
                            </TimelineItem>
                            <TimelineItem>
                                <TimelineSeparator>
                                    <TimelineDot variant='outlined' color="primary"/>
                                </TimelineSeparator>
                                <TimelineOppositeContent>Closed</TimelineOppositeContent>
                            </TimelineItem>
                        </Timeline>
                    </div>
                </div>
            </SimpleCard>
            <div className="py-3" />
            <SimpleCard title="Графическая оценка">
                <Grid container direction={"column"} spacing={3}>
                    <Grid item>
                        <div
                            className={`h-350 w-100% flex justify-center items-center`}
                        >
                            <ReactEcharts
                                style={{ height: '350px', width: '100%' }}
                                option={{
                                    ...option,
                                    color: [
                                        theme.palette.primary.main,
                                        theme.palette.primary.light,
                                    ],
                                }}
                            />
                        </div>
                        <div
                            className={`h-145 w-100% flex justify-center items-center`}
                        >
                            <Typography component="legend">Средняя оценка PR</Typography>
                            <Rating defaultValue={4.5} precision={0.5} size="large" readOnly/>
                        </div>
                    </Grid>
                </Grid>
            </SimpleCard>
        </div>
    )
}

export default View
