import React from 'react'

const prRoutes = [
    {
        path: '/pr/view/:during_pr_id',
        component: React.lazy(() => import('./View')),
    },
    {
        path: '/pr/addup/:during_pr_id',
        component: React.lazy(() => import('./addUpSr')),
    },
    {
        path: '/pr/leaveReview/:during_pr_id',
        component: React.lazy(() => import('./leaveReview')),
    },
]

export default prRoutes
